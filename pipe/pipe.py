import os
import json
import glob
from pathlib import Path
from random import randrange
import yaml
import requests
from requests.auth import HTTPBasicAuth

from bitbucket_pipes_toolkit import Pipe, get_logger

# Import WebClient from Python SDK (github.com/slackapi/python-slack-sdk)
from slack_sdk import WebClient
from slack_sdk.errors import SlackApiError

logger = get_logger()

schema = {
    'GIPHY_TOKEN': {'type': 'string', 'required': True},
    'SLACK_BEARER_TOKEN': {'type': 'string', 'required': True},
    'SLACK_CHANNEL_ID': {'type': 'string', 'required': True},
    'CHANGELOG_URL': {'type': 'string', 'required': True},
    'FILENAME': {'type': 'string', 'required': True},
    'VERSION': {'type': 'string', 'required': True},
    'BITBUCKET_REPO_FULL_NAME': {'type': 'string', 'default': 'BITBUCKET_REPO_FULL_NAME'},
    'BITBUCKET_BUILD_NUMBER': {'type': 'number', 'default': 0},
    'DEBUG': {'type': 'boolean', 'required': False, 'default': False}
}


class UploadFilePipe(Pipe):

    def run(self):
        super().run()
        logger.info('Executing the pipe...')

        GIPHY_TOKEN = self.get_variable('GIPHY_TOKEN')
        SLACK_BEARER_TOKEN = self.get_variable('SLACK_BEARER_TOKEN')
        SLACK_CHANNEL_ID = self.get_variable('SLACK_CHANNEL_ID')
        CHANGELOG_URL = self.get_variable('CHANGELOG_URL')
        FILENAME = self.get_variable('FILENAME')
        VERSION = self.get_variable('VERSION')
        BITBUCKET_BUILD_NUMBER = self.get_variable('BITBUCKET_BUILD_NUMBER')
        BITBUCKET_REPO_FULL_NAME = self.get_variable('BITBUCKET_REPO_FULL_NAME')


        try:
            offset = randrange(0, 1000)
            response = requests.get('http://api.giphy.com/v1/gifs/search?q=work&limit=1&offset=' + str(offset) + '&api_key=' + GIPHY_TOKEN)

            client = WebClient(token=SLACK_BEARER_TOKEN)

            try:
                blocks = [
                    {
                        "type": "section",
                        "text": {
                            "type": "mrkdwn",
                            "text": ":rocket: Pipeline #" + str(BITBUCKET_BUILD_NUMBER) + " is working on development build *" + VERSION + "*"
                        }
                    },
                    {
                        "type": "actions",
                        "elements": [
                            {
                                "type": "button",
                                "style": "primary",
                                "text": {
                                    "type": "plain_text",
                                    "text": "View Changelog",
                                    "emoji": True
                                },
                                "url": CHANGELOG_URL
                            },
                            {
                                "type": "button",
                                "text": {
                                    "type": "plain_text",
                                    "text": "View Pipeline",
                                    "emoji": True
                                },
                                "url": "https://bitbucket.org/" + BITBUCKET_REPO_FULL_NAME + "/addon/pipelines/home#!/results/" + str(BITBUCKET_BUILD_NUMBER)
                            },
                        ]
                    },
                    {
                        "type": "image",
                        "image_url": response.json()['data'][0]['images']['downsized']['url'],
                        "alt_text": "RELEASED!"
                    }
                ]


                # Call the chat.postMessage method using the WebClient
                result = client.chat_postMessage(
                    channel=SLACK_CHANNEL_ID, 
                    blocks=json.dumps(blocks)
                )
                logger.info(result)

                try:
                    # Call the files.upload method using the WebClient
                    # Uploading files requires the `files:write` scope
                    result_file = client.files_upload(
                        file=FILENAME,
                        channels=SLACK_CHANNEL_ID, 
                        initial_comment="Here we goooo.. :rocket::rocket::rocket:",
                        thread_ts=result['ts']
                    )
                    # Log the result
                    logger.info(result_file)

                except SlackApiError as e:
                    self.fail("Error uploading file: {}".format(e))

            except SlackApiError as e:
                self.fail(f"Error posting message: {e}")

        except Exception as e:
            self.fail(e);



if __name__ == '__main__':
    metadata = yaml.safe_load(open('/pipe.yml', 'r'))
    pipe = UploadFilePipe(schema=schema, pipe_metadata=metadata, check_for_newer_version=True)
    pipe.run()
